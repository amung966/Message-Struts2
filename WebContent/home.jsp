<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sb" uri="/struts-bootstrap-tags" %>
<%@ page import ="java.util.*" %>
<%@ page import="java.sql.*"%> 
<%@ page import="java.text.*"%>
<%@ page import="com.Struts2.Model.Message"%>
<%@ page import="com.Struts2.Server.MessageService"%> 
<%
	String logined = (String) session.getAttribute("logined");
	if(logined != null){
		if("logout".equals(logined)){
			response.setHeader("Refresh", "0; " + "index.jsp");
			return;
		}
	}else{
		response.setHeader("Refresh", "0; " + "index.jsp");
		return;
	}
	String uid = (String) session.getAttribute("uid");

%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Yi's Message Board</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7-->
  <link rel="stylesheet" href="adminLte/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="adminLte/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="adminLte/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="adminLte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="adminLte/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="adminLte/dist/css/skins/_all-skins.min.css">

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <!-- jQuery 3 -->
  <script src="adminLte/bower_components/jquery/dist/jquery.min.js"></script>
  <style type="text/css">
    .mbox{
      z-index: 100;
      position: absolute;
      width: 30%;
    }
  </style>
  <script type="text/javascript">
    jQuery(document).ready(function(){
      //Ã¤Â¸ÂÃ§Â®Â¡Ã¨Â¨ÂÃ¦ÂÂ¯Ã¨Â¦ÂÃ§ÂªÂÃ¦ÂÂ¯Ã¥ÂÂ¦Ã¦ÂÂÃ¥ÂÂÃ¥ÂÂÃ¯Â¼ÂÃ©Â ÂÃ©ÂÂ¢Ã¨Â¼ÂÃ¥ÂÂ¥2Ã§Â§ÂÃ¥Â¾ÂÃ©ÂÂ½Ã©ÂÂÃ©ÂÂ
      setTimeout(function(){
        $('.mbox').fadeOut();
      }, 2000);
    });
  </script>
</head>
<body class="hold-transition skin-blue sidebar-mini">

<div class="wrapper">
  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>LT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Admin</b>LTE</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="adminLte/dist/img/avatar5.png" class="user-image" alt="User Image">
              <span class="hidden-xs">user</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="adminLte/dist/img/avatar5.png" class="img-circle" alt="User Image">
                <p>
                  user
                </p>
              </li>
              <!-- Menu Body -->
              <!-- Menu Footer-->
              <li class="user-footer">
              	<div class="pull-left">
                  <a href="setting.jsp" class="btn btn-default btn-flat">Setting</a>
                </div>
                <div class="pull-right">
                  <a href="logout?uid=<% out.write((String) session.getAttribute("uid"));%>" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
<!--           <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
          </ul> -->
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  	<%
		String error = (String) session.getAttribute("error");
		if(error != null){
			out.write("<div class='alert alert-warning alert-dismissible mbox'>");
			out.write("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>");
			out.write("<h4><i class='icon fa fa-warning'></i> warning!</h4>");
			out.write(error);
			out.write("</div>");
			session.removeAttribute("error");
		}
		String message = (String) session.getAttribute("message");
		if(message != null){
			out.write("<div class='alert alert-success alert-dismissible mbox'>");
			out.write("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>");
			out.write("<h4><i class='icon fa fa-check'></i> success!</h4>");
			out.write(message);
			out.write("</div>");
			session.removeAttribute("message");
		}
		String failmsg = (String) session.getAttribute("failmsg");
		if(failmsg != null){
			out.write("<div class='alert alert-danger alert-dismissible mbox'>");
			out.write("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>");
			out.write("<h4><i class='icon fa fa-ban'></i> success!</h4>");
			out.write(failmsg);
			out.write("</div>");
			session.removeAttribute("failmsg");
		}
	%>
    <!-- Main content -->
    <section class="content">
   	  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
		        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        	<h4 class="modal-title" id="myModalLabel" style="float: left;">fix message</h4>
		      	</div>
		      	<div class="modal-body">
					<div class="box-body">
						<form action="Msg" method="POST" files="true" enctype="multipart/form-data">
							<input type="text" name="title" id ="ftit" placeholder="Message Title" class="form-control" required>
				            	<div class="input-group" id="msg_footer">
				          		<input type="text" name="info" id="finfo" placeholder="Some Message" class="form-control" required>
				             	<span class="input-group-btn">
				                <button id="msg_submit" class="btn btn-primary btn-flat">Send</button>
				              	</span>
					        	</div>
					        	<div id="file_upd">
					        		<input type="file" name="upload">
					        	</div>
					        <p id="moref_upd" class="btn bg-navy btn-sm">more file</p>
					        	<div id="file_d_p">
					        	</div>
			          		<input type="hidden" name="ofilename" id="ffile" required>
			          		<input type="hidden" name="tid" id="fwho" required>
			          		<input type="hidden" name="type" value="2" required>
				        	</form>
		            </div>
		      	</div>
		    </div>
		  </div>
	  </div>
      <div class="row">
        <div class="col-xs-12">
        		<div class="box-body"> 
        			<form action="Msg" method="POST" files="true" enctype="multipart/form-data">
	        			<input type="hidden" value="<% out.write((String) session.getAttribute("uid"));%>" name="user" required>
		            	<input type="hidden" value="1" name="type" required>
		            	<input type="text" name="title"  placeholder="Message Title" class="form-control" required>
		            	<div class="input-group" id="msg_footer">
		          		<input type="text" name="info" id="info" placeholder="Some Message" class="form-control" required>
		             	<span class="input-group-btn">
		                <button id="msg_submit" class="btn btn-primary btn-flat">Send</button>
		              	</span>
			        	</div>
			        	<div id="file_add">
			        		<input type="file" name="upload">
			        	</div>
			        <p id="moref_add" class="btn bg-navy btn-sm">more file</p>
		        	</form>
		        	<br>
		        	<%
		        		Message msg = new Message();
		        		MessageService dbmsg = new MessageService();
		        		
		        		msg = dbmsg.getMessage();
		        		String [] uname = msg.getUname();
		        		String [] date = msg.getDate();
		        		String [] tid = msg.getTid();
		        		String [] mesg = msg.getMsg();	
		        		String [] aid = msg.getAid();
		        		String [] title = msg.getTitle();
		        		
		        		String [] fid = msg.getFid();
		        		String [] ftid = msg.getFtid();
		        		String [] file = msg.getFile();
		        		String [] ofile = msg.getoFile();
		        		
		        		for(int i=0; i<uname.length; i++){
		        	%>
		               		
			    	<div class="direct-chat-msg">
			    		<div class="direct-chat-info clearfix">
			    			<span class="direct-chat-name pull-left">
			    				<% out.write(uname[i]); %>
			    			</span>
			    			<span class="direct-chat-timestamp pull-right">
			    				<% 
				    				if(uid.equals(aid[i])){
				    					out.write("<a class='fix'>fix this message <p style='display:none;'>"+tid[i]+"</p></a>");
				    				}
			    					out.write("&nbsp;&nbsp;");
			    					out.write(date[i]); 
			    				
			    				%>
			    			</span>
			    		</div>
			    		<img class="direct-chat-img" src="adminLte/dist/img/avatar5.png" alt="Message User Image">
			    		<div class="direct-chat-text">
			    			<s:url action="Msg" escapeAmp="false" var="urlTag">
						    <s:param name="tid"><% out.write(tid[i]); %></s:param>
						    <s:param name="type" value="3" />
						    <s:param name="info">1</s:param>
						</s:url>
						<a href="<s:property value="#urlTag" />"
						<%
							if(!uid.equals(aid[i])){
								out.write("style='display:none'");
							}
						%>
						class="close btn_del"><span aria-hidden="true">&times;</span></a>

		    				<%
		    					out.write("<p id='title_"+tid[i]+"'><b>"+title[i]+"</b></p>"); 
		    					out.write("<p id='info_"+tid[i]+"'>"+mesg[i]+"</p>"); 
		    					for(int j=0; j<fid.length; j++){
		    						if(tid[i].equals(ftid[j])){
		    							out.write("<a id='ofile_"+tid[i]+"_"+j+"' href='Download.action?type=5&info="+file[j]+"' target='block'>"+ofile[j]+"</a>");
		    							out.write("</br>");
		    							out.write("<p id='file_"+tid[i]+"_"+j+"' class='filec_"+tid[i]+"' style='display:none;'>"+file[j]+"</p>");
		    							out.write("<p id='ofile_"+tid[i]+"_"+j+"' style='display:none;'>"+ofile[j]+"</p>");
		    							out.write("<p id='fid_"+tid[i]+"_"+j+"' style='display:none;'>"+fid[j]+"</p>");
		    						}
		    					}
		    				%>

				    		
			    		</div>
			    	</div>
			    	<%
		        		}
			    	%>
	        </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer hidden-print">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0.1<br>
    </div>
    <strong>Copyright &copy; 2017 PowerBy Yi</strong> All rights reserved. <br>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<!-- Bootstrap 3.3.7 -->
<script src="adminLte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="adminLte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="adminLte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="adminLte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="adminLte/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="adminLte/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="adminLte/dist/js/demo.js"></script>
<!-- page script -->
<script>
	$(document).ready(function(){
		$(".btn_del").click(function(){
			var del = confirm('Delete it ?');
			if(!del){
				window.event.returnValue=false;
				return;
			}
		});
	    $(".fix").click(function(){
	    		var who = $(this).children('p:first').text();
	    		var info = $("#info_"+who).text();
	    		var cou = $(".filec_"+who).length;
	    		console.log(cou);
	    		var tit = $("#title_"+who).text();
	    		$("#file_d_p").html('');
	    		var file , ofile , fid;
	    		for(i=0;i<cou;i++){
	    			file = $("#file_"+who+"_"+i).text();
	    			ofile = $("#ofile_"+who+"_"+i).text();
	    			fid = $("#fid_"+who+"_"+i).text();
	    			console.log(ofile[i]);
	    			$('<p>'+ofile+'</p><a class="btn btn-sm btn-danger" href="Msg.action?type=4&info='+file+'&tid='+fid+'">del</a>').appendTo("#file_d_p");
	    		}
	    		console.log(file);
	    		console.log($("#file_"+who+"_1"));
	    		$("#finfo").val(info);
	    		$("#ftit").val(tit);
	    		$("#fwho").val(who);
	    		$('#myModal').modal('show');
	    });
	    $("#moref_add").click(function(){
			$('<input type="file" name="upload">').appendTo("#file_add");
		});
	    $("#moref_upd").click(function(){
			$('<input type="file" name="upload">').appendTo("#file_upd");
		});
	});
</script>
</body>
</html>
