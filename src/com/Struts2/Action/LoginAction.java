package com.Struts2.Action;

import java.util.Map;

import com.Struts2.Model.Users;
import com.Struts2.Server.UserService;
import com.Struts2.Server.MyClass;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class LoginAction extends ActionSupport{
	
	private String account , passwd;
	MyClass my = new MyClass();
	ActionContext ac = ActionContext.getContext();
	Map session = ac.getSession();
	
	public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }
    
	public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }
    
    public String execute() throws Exception{
    		System.out.println(account);
    		System.out.println(passwd);
    		
    		Users user = new Users();
    		UserService dbuser = new UserService();
    		
    		user = dbuser.hasuser(account, my.MD5(passwd));
    		if(user.getUid() == null) {
    			this.addFieldError("account" , "account or password error");
    			return "input";
    		}else {
    			session.put("logined", "logined");
    			session.put("uid" , user.getUid());
    			session.put("name" , user.getname());
    		}
        return "success";
    }
    
    @Override
    public void validate(){
    		super.validate();
    		if(getAccount() == null || "".equals(getAccount())) {
    			this.addFieldError("account", "Login account was required");
    		}
    		if(getPasswd() == null || "".equals(getPasswd())) {
    			this.addFieldError("passwd", "Login passwd was required");
    		}
    }
}
