package com.Struts2.Action;

import java.util.Map;

import com.Struts2.Server.MessageService;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class LogoutAction extends ActionSupport{
	
	private String uid;
	ActionContext ac = ActionContext.getContext();
	Map session = ac.getSession();
	
	public void setUid(String uid) {
        this.uid = uid;
    }
	
	public String getUid() {
        return uid;
    }
	
	public String execute() throws Exception{
		
		session.clear();
		
		return "success";
		
	}
	
	@Override
    public void validate(){
    		super.validate();
    		if(getUid() == null || "".equals(getUid())) {
    			this.addFieldError("uid", "Uid was required");
    		}

    }

}
