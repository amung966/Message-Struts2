package com.Struts2.Action;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.struts2.ServletActionContext;

import com.Struts2.Model.Message;
import com.Struts2.Server.MessageService;
import com.Struts2.Server.MyClass;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class MsgAction extends ActionSupport{
	private String title , info , user , type , tid , ofilename , tofile;
	private File[] uploads;
    private String[] uploadFileNames;
    private String[] uploadContentTypes;
    
	MyClass my = new MyClass();
	ActionContext ac = ActionContext.getContext();
	Map session = ac.getSession();
	Long tss = System.currentTimeMillis()/1000;
	String ts = Long.toString(tss);
	Message msg = new Message();
	MessageService dbmsg = new MessageService();
	
	//download file <!--
	private InputStream inputStream;
	private String downFileName;
	private long contentLength;
	//download file --!>
	//download file <!--
	public String getFileName() {
        return downFileName;
    }
	public long getContentLength() {
        return contentLength;
    }
 
    public InputStream getInputStream() {
        return inputStream;
    }  
  //download file --!>
	public void setType(String type) {
        this.type = type;
    }
	
	public void setTid(String tid) {
        this.tid = tid;
    }
	
	public void setTitle(String title) {
        this.title = title;
    }
	
	public void setInfo(String info) {
        this.info = info;
    }
	
	public void setUser(String user) {
        this.user = user;
    }
	
	public void setOfilename(String ofilename) {
        this.ofilename = ofilename;
    }
	
	public String getType() {
        return type;
    }
	
	public String getTid() {
        return tid;
    }
	
	public String getTitle() {
        return title;
    }
	
	public String getInfo() {
        return info;
    }
	
	public String getUser() {
        return user;
    }
	
	public String getOfilename() {
        return ofilename;
    }
	
	public void setTofile(String tofile) {
        this.tofile = tofile;
    }
	
	public String getTofile() {
        return tofile;
    }
	
	// file upload <!--
	public File[] getUpload() {
        return this.uploads;
    }
    public void setUpload(File[] upload) {
        this.uploads = upload;
    }
    public String[] getUploadFileName() {
        return this.uploadFileNames;
    }
    public void setUploadFileName(String[] uploadFileName) {
        this.uploadFileNames = uploadFileName;
    }
    public String[] getUploadContentType() {
        return this.uploadContentTypes;
    }
    public void setUploadContentType(String[] uploadContentType) {
        this.uploadContentTypes = uploadContentType;
    }
     
	public String getFileExtension(String sfilename) {
		String[] formatName = sfilename.split("\\.");
		int cou = formatName.length - 1;
		return formatName[cou];
	}
	// file upload --!>
	public String execute() throws Exception{
		String response = "";
		System.out.println("execute");
		System.out.println(type);
		if("1".equals(getType())) {
			response = Add();
		}else if("2".equals(getType())) {
			response = Upd();
		}else if("3".equals(getType())) {
			response = Del();
		}else if("4".equals(getType())){
			response = DelFile();
		}else if("5".equals(getType())){
			response = DownloadFile();
		}else {
			this.addFieldError("info", "error");
			return "input";
		}
		
		return response;
	}
	
	@Override
    public void validate(){
    		super.validate();
    		if(getInfo() == null || "".equals(getInfo())) {
    			this.addFieldError("info", "Info was required");
    		}

    }
	
	private String uploadFile(File upFile , String upfilename) {
		
		String newfilename = (String)ts.subSequence(5,9)+my.randomString(5)+"."+getFileExtension(upfilename);
		
		String target = ServletActionContext.getServletContext().getRealPath("/upload/"+newfilename);
		System.out.println(target);
		File targetFile=new File(target);
		
		try {
			FileUtils.copyFile(upFile, targetFile);
			return newfilename;
		}catch(IOException e) {
			System.out.println("up error:"+e.getMessage());
			return "";
		}
		
	}
	
	private String Add() {
		System.out.println("add");
		String [] sfilename = getUploadFileName();
		File [] allFile = getUpload();
		String [] file = null;
		if(sfilename != null) {
			file = new String[allFile.length];
			for(int i=0; i<allFile.length ;i++) {
				file[i] = uploadFile(allFile[i] , sfilename[i]);
			}
		}else {
			file = null;
		}
		
		boolean status = dbmsg.insertMsg(getUser() , getInfo() , getTitle() , file , sfilename);
		
		if(!status) {
			this.addFieldError("info", "Insert error");
			return "input";
		}
		
		return "success";
	}
	
	private String Upd() {
		System.out.println("upd");
		String [] sfilename = getUploadFileName();
		File [] allFile = getUpload();
		String [] file = null;
		if(sfilename != null) {
			file = new String[allFile.length];
			for(int i=0; i<allFile.length ;i++) {
				file[i] = uploadFile(allFile[i] , sfilename[i]);
			}
		}else {
			file = null;
		}
		
		boolean status = dbmsg.updMsg(getTid(), getInfo() , getTitle() , file , sfilename);
		
		if(!status) {
			this.addFieldError("info", "Update error");
			return "input";
		}
		
		return "success";
	}
	
	private String Del() {
		System.out.println("del");
		
		boolean status = dbmsg.delMsg(getTid());
		if(!status) {
			return "input";
		}
		return "success";
	}
	
	private String DelFile() {
		System.out.println("del file");
		if("1".equals(getInfo())) {
			return "input";
		}
		String target = ServletActionContext.getServletContext().getRealPath("/upload/"+getInfo());
		my.deleteFile(target);
		
		boolean status = dbmsg.delFile(getTid());
		if(!status) {
			return "input";
		}
		return "success";
	}
	
	public String DownloadFile() {
		System.out.println("download file");
		try {
			File fileToDownload = new File(ServletActionContext.getServletContext().getRealPath("/upload/"+getInfo()));
			inputStream = new FileInputStream(fileToDownload);
			downFileName = fileToDownload.getName();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		return "success";
	}

}
