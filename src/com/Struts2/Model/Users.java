package com.Struts2.Model;

public class Users {
	
	private String uid;
	private String account;
	private String passwd;
	private String name;
	
	public Users() {
		super();
	}
	
	public Users(String uid , String account , String passwd , String name) {
		super();
		this.uid = uid;
		this.account = account;
		this.passwd = passwd;
		this.name = name;
	}
	
	public Users(String account , String passwd) {
		super();
		this.account = account;
		this.passwd = passwd;
	}
	
	public String getUid() {
		return uid; 
	}

	public String getAccount() {
		return account; 
	}
	
	public String getPasswd() {
		return passwd; 
	}
	
	public String getname() {
		return name; 
	}
	
	public void setUid(String uid) {
		this.uid = uid; 
	}
	
	public void setAccount(String account) {
		this.account = account; 
	}
	
	public void setPasswd(String passwd) {
		this.passwd = passwd; 
	}
	
	public void setName(String name) {
		this.name = name; 
	}
}
