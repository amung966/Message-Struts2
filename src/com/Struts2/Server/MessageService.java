package com.Struts2.Server;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;

import org.apache.struts2.ServletActionContext;

import com.Struts2.Model.Message;
import com.mysql.jdbc.Connection;

public class MessageService {
	
	private DBConnection db;
	private MyClass my;
	private Connection con;
	private PreparedStatement ps;
	private ResultSet rs;
	private Long tss;
	private String ts;
	
	
	public MessageService() {
		db = new DBConnection();
		my = new MyClass();
		con = db.getConnection();
		ps = null;
		rs = null;
	    tss = System.currentTimeMillis()/1000;
		ts = Long.toString(tss);
	}
	
	public Message getMessage() {
		Message message = new Message();
		String []tid , msg , date , uname , file , ofile , aid , title , fid , ftid;
		
		String sql = "SELECT * FROM message_view";
		
		try {
			
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			rs.last();
			int cou = rs.getRow();
			
			tid = new String[cou];
			msg = new String[cou];
			date = new String[cou];
			uname = new String[cou];
			aid = new String[cou];
			title = new String[cou];
			
			rs.first();
			for(int i=0;i<cou;i++) {
				tid[i] = rs.getString(1);
				msg[i] = rs.getString(3);
				date[i] = my.Date(Long.parseLong(rs.getString(5)));
				uname[i] = rs.getString(11);
				aid[i] = rs.getString(2);
				title[i] = rs.getString(4);
				rs.next();
			}
			
			sql = "SELECT * FROM file";

			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			rs.last();
			int couu = rs.getRow();
			
			fid = new String[couu];
			ftid = new String[couu];
			file = new String[couu];
			ofile = new String[couu];
			rs.first();
			for(int i=0; i<couu; i++) {
				fid[i] = rs.getString(1);
				ftid[i] = rs.getString(2);
				file[i] = rs.getString(3);
				ofile[i] = rs.getString(4);
				rs.next();
			}
			
			message.setTid(tid);
			message.setMsg(msg);
			message.setDate(date);
			message.setUname(uname);
			message.setAid(aid);
			message.setTitle(title);
			message.setFid(fid);
			message.setFtid(ftid);
			message.setFile(file);
			message.setoFile(ofile);
			
			
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			db.closeDB(con , rs , ps);
		}
		
		return message;
	}
	
	public boolean insertMsg(String user , String msg , String title , String file[] , String ofile[]) {
		
		String sql = "INSERT INTO message (t_a_id , t_info , t_title ,  t_created , t_changed , t_lasttime) "
				+ "VALUES (?,?,?,?,?,?)";
		
		try {
			ps = con.prepareStatement(sql);
			ps.setString(1, user);
			ps.setString(2, msg);
			ps.setString(3, title);
			ps.setString(4, ts);
			ps.setString(5, ts);
			ps.setString(6, ts);
			ps.executeUpdate();
			
			sql = "SELECT t_id FROM message_view WHERE t_created = ?";
			ps = con.prepareStatement(sql);
			ps.setString(1, ts);
			rs = ps.executeQuery();
			rs.last();
			String tid = rs.getString(1);
			
			if(file != null) {
				boolean status = insertFile(tid, file , ofile);
				if(!status) {
					delMsg(tid);
					return false;
				}
				
			}
			
			
			return true;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
	}
	
	public boolean updMsg(String tid , String msg , String title , String file[] , String ofile[]) {
		
		String sql = "UPDATE message SET t_info = ? , t_title = ? , t_changed = ? , t_lasttime = ?" + " WHERE t_id = ?";
		
		try {
			
			ps = con.prepareStatement(sql);
			ps.setString(1, msg);
			ps.setString(2, title);
			ps.setString(3, ts);
			ps.setString(4, ts);
			ps.setString(5, tid);
			ps.executeUpdate();
			
			if(file != null) {
				boolean status = insertFile(tid, file , ofile);
				if(!status) {
					return false;
				}
				
			}
			
			return true;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
	}
	
	public boolean delMsg(String tid) {
		
		String sql = "DELETE FROM message where t_id = ?";
		
		try {
			
			ps = con.prepareStatement(sql);
			ps.setString(1, tid);
			ps.executeUpdate();
			
			sql = "SELECT * FROM file WHERE f_t_id ="+tid;
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			rs.last();
			int cou = rs.getRow();
			rs.first();
			for(int i=0;i<cou;i++) {
				String target = ServletActionContext.getServletContext().getRealPath("/upload/"+rs.getString(4));
				my.deleteFile(target);
				rs.next();
			}
			
			sql = "DELETE FROM file where f_t_id = ?";
			ps = con.prepareStatement(sql);
			ps.setString(1, tid);
			ps.executeUpdate();
			
			return true;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
	}
	
	private boolean insertFile(String tid , String file[] , String ofile[]) {
		
		String sql = "INSERT INTO file (f_t_id , f_name , f_o_name , f_created , f_changed , f_lasttime)"
				+ "VALUES (?,?,?,?,?,?)";
		
		for(int i=0; i<file.length;i++) {
			
			try {
				ps = con.prepareStatement(sql);
				ps.setString(1, tid);
				ps.setString(2, file[i]);
				ps.setString(3, ofile[i]);
				ps.setString(4, ts);
				ps.setString(5, ts);
				ps.setString(6, ts);
				ps.executeUpdate();
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}
			
		}
			
		return true;
		
	}
	
	public boolean delFile(String fid) {
		
		System.out.println(fid);
		String sql = "DELETE FROM file where f_id = ?";
		
		try {
			
			ps = con.prepareStatement(sql);
			ps.setString(1, fid);
			ps.executeUpdate();
			
			return true;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
}
