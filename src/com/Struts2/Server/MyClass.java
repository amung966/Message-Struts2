package com.Struts2.Server;

import java.io.File;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import com.sun.jmx.snmp.Timestamp;

public class MyClass {
	
	public MyClass() {
		
	}
	public String MD5(String str) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(str.getBytes());
			str = new BigInteger(1, md.digest()).toString(16);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			str = null;
			System.out.println(e.getMessage());
		}
		return str;
	}
	public String Date(long time) {
		time = time * 1000;
		String tsStr = "";  
		SimpleDateFormat format =  new SimpleDateFormat( "yyyy-MM-dd hh:mm" );
        try {  
            //方法一  
            tsStr = format.format(time);   
        } catch (Exception e) {  
            e.printStackTrace();  
        }
		return tsStr;  
		
	}
	public static String randomString(int len) { 
		String str = "0123456789abcdefghijklmnopqrstuvwxyz"; 
		StringBuffer sb = new StringBuffer(); 
		for (int i = 0; i < len; i++) { 
			int idx = (int)(Math.random() * str.length()); 
			sb.append(str.charAt(idx)); 
		} 
		return sb.toString(); 
	}
	public boolean deleteFile(String file_url) {
		try {
			File file = new File(file_url);
			file.delete();
			return true;
		}catch(Exception e) {
			System.out.println(e.getMessage());
			return false;
		}

		
	}
}
